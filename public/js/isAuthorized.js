firebase.auth().onAuthStateChanged(function(user) {
    if (!user) {
        $(location).attr('href', '../index.html');
    }
});

function signOut() {
    firebase.auth().signOut().then(function() {
        // Sign-out successful.
    }).catch(function(error) {
        // An error happened.
    });
}

// init Google API for Google Sheets connection
function initGapi() {
    if (gapi.auth2.getAuthInstance() != null) {
        return;
    }
    gapi.client.init({
        apiKey: apiKey,
        clientId: clientId,
        discoveryDocs: DISCOVERY_DOCS,
        scope: SCOPES
    }).then(function () {
        if (gapi.auth2.getAuthInstance().isSignedIn.get()) {
        } else {
            firebase.auth().signOut(); // Something went wrong, sign out
        }
    });
}

function formatDate(date) {
    return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
}

function formatTime(time) {
    return time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
}

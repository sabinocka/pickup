let driverURL = spreadsheetURL + "Vodiči!A2:D?key=" + apiKey;

var now = "";
var curUser = "";
var postKey = "";

(function ($) {
    $(document).ready(function () {
        now = new Date();
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                initGapi();

                curUser = user.email;
                var today = formatDate(now);
                $('#profileInfo').text("Vodič " + curUser.substr(0, curUser.indexOf("@")) + ", " + today);
                var profiles = firebase.database().ref('profiles');
                profiles.orderByChild("postData/user").equalTo(curUser).on('value', function(snapshot) {
                    if (snapshot.val() != null) {
                        snapshot.forEach(function(childSnapshot) {
                            postKey = childSnapshot.key;
                            var childData = childSnapshot.val();
                            $('#ecv').val(childData.postData.ecv);
                            $('#kms').val(childData.postData.kms);
                        });
                    }
                });
            } else {
                // No user is signed in.
            }
        });
    })
})(jQuery);

function submitForm() {
    var ecv = $('#ecv').val();
    var kms = $('#kms').val();

    var postData = {
        user: curUser,
        date: firebase.database.ServerValue.TIMESTAMP,
        ecv: ecv,
        kms: kms
    };

    if (postKey === "") {
        postKey = firebase.database().ref().child('profiles').push().key;
    }
    updateDriverSheet(postData);

     return firebase.database().ref('/profiles/' + postKey).set({
         postData
     }, function (error) {
     });

}

function formatDate(date) {
    return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
}

function updateDriverSheet(original) {
    let now = new Date();
    let writeName = original.user;
    let writeDate = formatDate(now) + " " + formatTime(now);
    let writeECV = original.ecv;
    let writeKms = original.kms;

    let url2 = "A1:D1";

    var body = {
        range: "Vodiči!" + url2,
        majorDimension: "ROWS",
        values: [[writeName, writeDate, writeECV, writeKms]]
    }

    gapi.client.sheets.spreadsheets.values.append({
        spreadsheetId: spreadsheetId,
        range: "Vodiči!" + url2,
        valueInputOption: "USER_ENTERED",
        resource: body
    }).then(() => {
    });
}

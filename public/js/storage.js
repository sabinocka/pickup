let containersURL = spreadsheetURL + "Kontajnery!A2:H?key=" + apiKey;
let subcategoriesURL = spreadsheetURL + "Úkony!E2:E?key=" + apiKey;
let categoriesURL = spreadsheetURL + "Úkony!D2:D?key=" + apiKey;
let garbageURL = spreadsheetURL + "Odpad!C2:D?key=" + apiKey;
let supplierURL = spreadsheetURL + "Dodávatelia!A1:B?key=" + apiKey;

var containersData = [];
var categoriesData = [];
var garbageData = [];
var supplierData = [];
var subcategoryData = [];
var data = {};
var rowCount = 0;
var curUser = "";
var editMode = false;

(function ($) {
    $(document).ready(function () {
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                initGapi();

                let records = firebase.database().ref('records');
                curUser = user.email;
                let today = formatDate(new Date());
                $('#info').text("Vodič " + curUser.substr(0, curUser.indexOf("@")) + ", " + today);

                //load data for current driver from firebase database
                records.orderByChild("postData/date").equalTo(today).once('value', function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        let childData = childSnapshot.val();
                        if (childData.postData.email === curUser) {
                            let key = "\'" + childSnapshot.key + "\'";
                            data[childSnapshot.key] = childData;
                            $('tbody')
                                .append($("<tr id='row'>" +
                                    "<td>" + childData.postData.container + "</td>" +
                                    "<td>" + childData.postData.customer + "</td>" +
                                    "<td>" + childData.postData.order + "</td>" +
                                    "<td>" + childData.postData.category + "</td>" +
                                    "<td>" + childData.postData.garbage + "</td>" +
                                    "<td>" + childData.postData.supplier + "</td>" +
                                    "<td>" + childData.postData.ending + "</td>" +
                                    "<td>" + childData.postData.finaldest + "</td>" +
                                    "<td>" + childData.postData.amount + "</td>" +
                                    '<td class=\'td-btn\'><button class="btn btn-primary btn-space" onclick="editRow(' + rowCount + ',' + key + ')" id="btn' + rowCount + '">Uprav</button>' +
                                    '<button class="btn btn-primary btn-space" onclick="deleteData(' + rowCount + ',' + key + ')" id="btn' + rowCount + '">Vymaž</button></td>' +
                                    "</tr>")
                                    .attr("id", "row" + rowCount++));
                            if (childData.postData.ending === 'Áno') {
                                $("#row" + (rowCount - 1)).hide();
                            }
                        }
                    });
                });
                loadJsons();
            } else {
            }
        });

        //set chechbox for finished/unfinished tasks
        $('#finishedTasks').change(function() {
            if(this.checked) {
                for (let i = 0; i < rowCount; i++) {
                    if ($("#row" + i + " td:eq(6)").text() === 'Áno'){
                        $("#row" + i).show();
                    }
                }
            } else {
                for (let i = 0; i < rowCount; i++) {
                    if ($("#row" + i + " td:eq(6)").text() === 'Áno'){
                        $("#row" + i).hide();
                    }
                }
            }
        });
    })
})(jQuery);

// add new row to table of loaded containers
function addRow() {
    if (editMode === true) {
        return;
    }
    editMode = true;
    $("#add").hide();
    var newRow = document.createElement('tr');
    newRow.setAttribute("id", "tmptr");
    newRow.innerHTML =
        "<td>" +
            "<input id='contInput' list='containerSelect'>" +
            "<datalist id=\"containerSelect\" class=\"selectpicker\"  style=\"width: 100px;\">" +
            "</datalist>" +
        "</td>" +
        "<td id=\"customer\"></td>" +
        "<td id=\"order\"></td>" +
        "<td>" +
            "<select id=\"categorySelect\" class=\"selectpicker\" name='category'>" +
            "</select>" +
        "</td>" +
        "<td>" +
            "<input id='garbInput' list='garbageSelect'>" +
            "<datalist id=\"garbageSelect\" class=\"selectpicker\">" +
            "</datalist>" +
        "</td>" +
        "<td>" +
            "<input id='suppInput' list='supplierSelect'>" +
            "<datalist id=\"supplierSelect\" class=\"selectpicker\">" +
            "</datalist>" +
        "</td>" +
        "<td id=\"ending\">" +
            "<p><input type=\"radio\" name=\"yes_no\" checked>Nie</input></p>" +
            "<p><input type=\"radio\" name=\"yes_no\">Áno</input></p>" +
        "</td>" +
        "<td id=\"finaldest\" contenteditable='true'></td>" +
        "<td id=\"amount\" contenteditable='true'></td>" +
        "<td class='td-btn'>" +
            "<button class=\"btn btn-primary btn-space\" onclick=\"submitRow()\">Pridať</button>" +
            "<button class=\"btn btn-primary btn-space\" onclick=\"cancelTmpRow()\">Zrušiť</button>" +
        "</td>";
    $("#storage").append(newRow);

    //set containers data
    $.each(containersData, function (key, item) {
        $('#containerSelect')
            .append($("<option></option>")
                .attr("data-value", key)
                .attr("value", item.cislo));
    });

    setGarbageData();
    setSupplierData();

    //set categories data
    $.each(categoriesData, function (key, item) {
        $('#categorySelect')
            .append($("<option></option>")
                .attr("value", key)
                .text(item));
    });

    //set actions over chosen value of container input
    $("#contInput").on('input', function () {
        let val = this.value;
        let position = $('#containerSelect [value="' + val + '"]').data('value');
        if($('#containerSelect option').filter(function () {
            return this.value.toUpperCase() === val.toUpperCase();
        }).length) {
            $("#customer").text(containersData[position].zakaznik.toString());
            $("#order").text(containersData[position].zakazka.toString());
            if (containersData[position].poznamka !== undefined && containersData[position].poznamka.includes("prerušený")) {
                $("#amount").text(containersData[position].hmotnost.toString());
                $("#garbInput").val(containersData[position].odpad.toString());
                if (containersData[position].poznamka.includes("vývoz")) {
                    $('#categorySelect').val(6);
                } else if (containersData[position].poznamka.includes("presun")) {
                    $('#categorySelect').val(4);
                } else {
                    $('#categorySelect').val(3);
                }
            }
        } else {
            $("#customer").text("");
            $("#order").text("");
            $("#amount").text("");
            $("#garbInput").val("");
            $('#categorySelect').val(0);
        }

    });
}

//load data from excel sheets to fill select dropdowns
function loadJsons() {
    $.ajaxSetup({
        async: false
    });

    $.getJSON(containersURL, function (data) {
        $.each(data.values, function(key, item) {
            containersData[key] = {
                zakaznik: item[0],
                zakazka: item[1],
                cislo: item[2],
                objem: item[3],
                pozicia: item[4],
                poznamka: item[5],
                odpad: item[6],
                hmotnost: item[7]
            };
        });
    });

    processDoubleColumn(garbageURL, garbageData);
    processDoubleColumn(supplierURL, supplierData);

    $.getJSON(categoriesURL, function (data) {
        $.each(data.values, function(key, item) {
            categoriesData[key] = item;
        });
    });

    $.getJSON(subcategoriesURL, function (data) {
        $.each(data.values, function(key, item) {
            subcategoryData[key] = item;
        });
    });

    $.ajaxSetup({
        async: true
    })
}

//format excel data that come in two columns into one
function processDoubleColumn(sheetURL, sheetData) {
    $.getJSON(sheetURL, function (data) {
        let size = 0;
        $.each(data.values, function(key, item) {
            if (item[0] !== "") {
                sheetData[key] = item[0];
                size++;
            }
        });
        $.each(data.values, function(key, item) {
            if (item[1] !== "") {
                sheetData[key + size] = item[1];
            }
        });
    });
}

//add currently edited row to table, submit to database & update excel sheets accordingly
function submitRow() {
    var user = firebase.auth().currentUser;
    var container = $('#contInput').val();
    var customer = $('#customer').text();
    var order = $('#order').text();
    var category = $('#categorySelect option:selected').text();
    var garbage = $('#garbInput').val();
    var supplier = $('#suppInput').val();
    var ending = $("#ending input[type='radio']:checked").parent('p').text();
    var finaldest = $('#finaldest').text();
    var amount = $('#amount').text();

    var postData = {
        email: user.email,
        container: container,
        customer: customer,
        order: order,
        category: category,
        garbage: garbage,
        supplier: supplier,
        ending: ending,
        finaldest: finaldest,
        amount: amount,
        date: formatDate(new Date())
    }

    var newPostKey = firebase.database().ref().child('records').push().key;
    var updates = {};
    updates['/records/' + newPostKey + '/postData'] = postData;

    return firebase.database().ref('/records/' + newPostKey).update({
        postData
    }, function (error) {
        if (error) {

        } else {
            cancelTmpRow();
            appendToLogSheet(postData);
            $('tbody')
                .append($("<tr id='row'>" +
                    "<td>" + postData.container + "</td>" +
                    "<td>" + postData.customer + "</td>" +
                    "<td>" + postData.order + "</td>" +
                    "<td>" + postData.category + "</td>" +
                    "<td>" + postData.garbage + "</td>" +
                    "<td>" + postData.supplier + "</td>" +
                    "<td>" + postData.ending + "</td>" +
                    "<td>" + postData.finaldest + "</td>" +
                    "<td>" + postData.amount + "</td>" +
                    '<td><button class="btn btn-primary btn-space" onclick="editRow(' + rowCount + ', \'' + newPostKey + '\')" id="btn' + rowCount + '">Uprav</button>' +
                    '<button class="btn btn-primary btn-space" onclick="deleteData(' + rowCount + ', \'' + newPostKey + '\')" id="btn' + rowCount + '">Vymaž</button></td>' +
                    "</tr>")
                    .attr("id", "row" + rowCount++));

            if (postData.category.includes("doprava") && postData.garbage !== "") {
                appendToLogSheet(postData);
                appendToMainSheet(postData);
            }
            if (postData.ending === 'Áno' && $('#finishedTasks').is(":checked") === false) {
                $("#row" + (rowCount - 1)).hide();
            } else {
                $("#row" + (rowCount - 1)).show();
            }

        }
    });
}

//cancel the editing mode
function cancelTmpRow() {
    $("#row" + rowCount + " td:lt(9)").attr('contenteditable', 'false');
    $("#tmptr").remove();
    $("#add").show();
    editMode = false;
}

//change existing row in table
function editRow(id, key) {
    if (editMode === true) {
        return;
    }
    editMode = true;
    $("#add").hide();
    $("#row" + id + " td:lt(9)").attr('contenteditable', 'true');
    var originalRow = {};
    originalRow.container = $("#row" + id + " td:eq(0)").text();
    originalRow.customer = $("#row" + id + " td:eq(1)").text();
    originalRow.order = $("#row" + id + " td:eq(2)").text();
    originalRow.category = $("#row" + id + " td:eq(3)").text();
    originalRow.garbage = $("#row" + id + " td:eq(4)").text();
    originalRow.supplier = $("#row" + id + " td:eq(5)").text();
    originalRow.ending = $("#row" + id + " td:eq(6)").text();
    originalRow.finaldest = $("#row" + id + " td:eq(7)").text();
    originalRow.amount = $("#row" + id + " td:eq(8)").text();

    $("#row" + id + " td:eq(0)").html(
        "<input id='contInput' list='containerSelect' value='" + originalRow.container  + "'>" +
        "<datalist id=\"containerSelect\" class=\"selectpicker\">" +
        "</datalist>"
    );
    $("#row" + id + " td:eq(3)").html(
        "<select id=\"categorySelect\" class=\"selectpicker\">" +
        "</select>"
    );
    $("#row" + id + " td:eq(4)").html(
        "<input id='garbInput' list='garbageSelect' value='" + originalRow.garbage  + "'>" +
        "<datalist id=\"garbageSelect\" class=\"selectpicker\">" +
        "</datalist>"
    );
    $("#row" + id + " td:eq(5)").html(
        "<input id='suppInput' list='supplierSelect' value='" + originalRow.supplier  + "'>" +
        "<datalist id=\"supplierSelect\" class=\"selectpicker\">" +
        "</datalist>"
    );
    $("#row" + id + " td:eq(6)").html(
        "<p><input type=\"radio\" name=\"yes_no\">Nie</input></p>" +
        "<p><input type=\"radio\" name=\"yes_no\">Áno</input></p>"
    );

    $.each(containersData, function (key, item) {
        $('#containerSelect')
            .append($("<option></option>")
                .attr("data-value", key)
                .attr("value", item.cislo));
    });

    $("#contInput").on('input', function () {
        var val = this.value;
        var position = $('#containerSelect [value="' + val + '"]').data('value');
        if($('#containerSelect option').filter(function () {
            return this.value.toUpperCase() === val.toUpperCase();
        }).length) {
            $("#row" + id + " td:eq(1)").text(containersData[position].zakaznik.toString());
            $("#row" + id + " td:eq(2)").text(containersData[position].zakazka.toString());
        }

    });

    setGarbageData();
    setSupplierData();

    $.each(categoriesData, function (key, item) {
        $('#categorySelect')
            .append($("<option></option>")
                .attr("value", key)
                .text(item));
    });

    $('#categorySelect option').filter(function () {
        return $(this).text() === originalRow.category;
    }).prop('selected',true);

    $('#categorySelect').change(function () {
        if ($('option:selected', $(this)).text() === "manipulačný presun") {
            setLeontech(id, 0);
        } else if ($('option:selected', $(this)).text().includes("prerušený")) {
            setLeontech(id, 1);
        }
    });

    if ($("#row" + id + " td:eq(6) input[type='radio']:eq(0)").parent('p').text() === originalRow.ending) {
        $("#row" + id + " td:eq(6) input[type='radio']:eq(0)").prop('checked', true);
    } else {
        $("#row" + id + " td:eq(6) input[type='radio']:eq(1)").prop('checked', true);
    }

    $('input[type=radio][name=yes_no]').on('change', function() {
        if ($("#categorySelect option:selected").text() === "dovoz materiálu" &&
            $("#row" + id + " td:eq(6) input[type=\'radio\']:checked").parent('p').text() === "Áno") {
            setLeontech(id);
        }
    });


    $("#row" + id + " td:eq(9) button:eq(0)").attr('onclick', 'updateRow(' + id + ',' + JSON.stringify(originalRow) + ', \'' + key + '\')').text("Zmeniť");
    $("#row" + id + " td:eq(9) button:eq(1)").attr('onclick', 'cancelChange(' + id + ',' + JSON.stringify(originalRow) + ', \'' + key + '\')').text("Zrušiť");
}

// set select for garbage data
function setGarbageData() {
    $.each(garbageData, function (key, item) {
        $('#garbageSelect')
            .append($("<option></option>")
                .attr("data-value", key)
                .attr("value", item));
    });

    $("#garbInput").on('input', function () {
        var val = this.value;
        if($('#garbageSelect option').filter(function () {
            return this.value.toUpperCase() === val.toUpperCase();
        }).length) {}
    });
}

//set select for supplier data
function setSupplierData() {
    $.each(supplierData, function (key, item) {
        $('#supplierSelect')
            .append($("<option></option>")
                .attr("data-value", key)
                .attr("value", item));
    })

    $("#suppInput").on('input', function () {
        var val = this.value;
        if($('#supplierSelect option').filter(function () {
            return this.value.toUpperCase() === val.toUpperCase();
        }).length) {}
    });
}

//set leontech as customer
function setLeontech(id, toChange) {
    if (toChange === 0) {
        $("#row" + id + " td:eq(1)").text("leontech");
        $("#row" + id + " td:eq(2)").html(
            "<select id=\"subcategorySelect\" class=\"selectpicker\">" +
            "</select>"
        );
    } else {
        $("#row" + id + " td:eq(7)").html(
            "<select id=\"subcategorySelect\" class=\"selectpicker\">" +
            "</select>"
        );
    }
    $.each(subcategoryData, function (key, item) {
        $('#subcategorySelect')
            .append($("<option></option>")
                .attr("value", key)
                .text(item));
    })
}

//revert changes done in edit mode in case of cancellation
function cancelChange(id, original, key) {
    $("#row" + id + " td:lt(9)").attr('contenteditable', 'false');
    $("#row" + id + " td:eq(0)").text(original.container);
    $("#row" + id + " td:eq(1)").text(original.customer);
    $("#row" + id + " td:eq(2)").text(original.order);
    $("#row" + id + " td:eq(3)").text(original.category);
    $("#row" + id + " td:eq(4)").text(original.garbage);
    $("#row" + id + " td:eq(5)").text(original.supplier);
    $("#row" + id + " td:eq(6)").text(original.ending);
    $("#row" + id + " td:eq(7)").text(original.finaldest);
    $("#row" + id + " td:eq(8)").text(original.amount);
    $("#row" + id + " td:eq(9) button:eq(0)").attr('onclick', 'editRow(' + id + ', \'' + key + '\')').text("Uprav");
    $("#row" + id + " td:eq(9) button:eq(1)").attr('onclick', 'deleteData(' + id + ', \'' + key + '\')').text("Vymaž");

    $("#add").show();
    editMode = false;
}

// change currently updated row in firebase database and change corresponding data in excel sheets
function updateRow(id, original, key) {
    var user = firebase.auth().currentUser;
    var postData = {
        email: user.email,
        container: $('#contInput').val(),
        customer: $("#row" + id + " td:eq(1)").text(),
        order: $("#row" + id + " td:eq(2)").text(),
        category: $("#categorySelect option:selected").text(),
        garbage: $('#garbInput').val(),
        supplier: $('#suppInput').val(),
        ending: $("#row" + id + " td:eq(6) input[type=\'radio\']:checked").parent('p').text(),
        finaldest: $("#row" + id + " td:eq(7)").text(),
        amount: $("#row" + id + " td:eq(8)").text(),
        date: formatDate(new Date())
    }

    updateSheet(postData, postData);
    if (postData.category === "manipulačný presun" ||
        (postData.category === "dovoz materiálu" && postData.ending === "Áno")) {
        postData.order = $("#subcategorySelect option:selected").text();
    }

    if (postData.category.includes("prerušený")) {
        postData.finaldest = $("#subcategorySelect option:selected").text();
    }

    if (postData.category === "pristavenie" && postData.finaldest === "" ) {
        postData.finaldest = postData.order;
        $("#row" + id + " td:eq(7)").text(postData.order.toString());
    }

    if (postData.category === "presun" && postData.finaldest !== "") {
        postData.order = postData.finaldest;
        $("#row" + id + " td:eq(2)").text(postData.finaldest);
    }

    if (original.customer !== postData.customer ||
        original.order !== postData.order ||
        original.destination !== postData.destination ||
        postData.category.includes("prerušený"))
    {
        updateSheet(original, postData);
    }
    if (postData.ending === 'Áno' && $('#finishedTasks').is(":checked") === false) {
        $("#row" + id).hide();
    } else {
        $("#row" + id).show();
    }

    if ((postData.ending === "Áno" && postData.category !== "presun") ||
        (original.garbage.toString() !== postData.garbage.toString() && (postData.category.includes("doprava")))
    ) {
        appendToMainSheet(postData);
    }
    appendToLogSheet(postData);

    var updates = {};
    updates['/records/' + key] = postData;

    return firebase.database().ref('/records/' + key).set({
        postData
    }, function (error) {
        if (error) {

        } else {
            cancelChange(id, postData, key);
        }
    });
}

//update data in excel sheets
function updateSheet(original, changed) {
    let current = -1;
    let writeCustomer = null;
    let writeOrder = null;
    let writeDestination = null;
    let writeGarbage = "";
    let writeAmount = "";
    let writeNote = "";


    $.grep(containersData, function (obj, key) {
        if (obj.cislo === changed.container) {
            current = key + 2;
        }
    });
    var url2 = "A" + current + ":H" + current;

    containersData[current - 2].zakaznik = changed.customer;
    containersData[current - 2].zakazka = changed.order;
    containersData[current - 2].pozicia = changed.destination;

    if (original.customer !== changed.customer) {
        writeCustomer = changed.customer;
    }
    if (original.order !== changed.order) {
        writeOrder = changed.order;
    }
    if (original.finaldest !== changed.finaldest) {
        writeDestination = changed.finaldest;
    }
    if (original.category.includes("prerušený") || changed.category.includes("prerušený")) {
        writeNote = original.category;
        writeGarbage = original.garbage;
        writeAmount = original.amount;
        containersData[current - 2].poznamka = "";
        containersData[current - 2].odpad = "";
        containersData[current - 2].hmotnost = "";
        if (original.garbage !== changed.garbage) {
            writeGarbage = changed.garbage;
        }
        if (original.amount !== changed.amount) {
            writeAmount = changed.amount;
        }
    }

    var body = {
        range: "Kontajnery!" + url2,
        majorDimension: "ROWS",
        values: [[writeCustomer, writeOrder, null, null, writeDestination,
            writeNote, writeGarbage, writeAmount]]
    }

    gapi.client.sheets.spreadsheets.values.update({
                spreadsheetId: spreadsheetId,
                range: "Kontajnery!" + url2,
                valueInputOption: "USER_ENTERED",
                resource: body
            }).then(() => {
            });
}

//append row to excel sheet
function appendToSheet(postData, sheetName) {
    var body = {
        range: sheetName + "!A1:J1",
        majorDimension: "ROWS",
        values: [[
            postData.date + " " + formatTime(new Date()),
            postData.customer,
            postData.order,
            postData.container,
            curUser.substr(0, curUser.indexOf("@")),
            postData.category,
            postData.garbage,
            postData.supplier,
            postData.finaldest,
            postData.amount
        ]]
    }

    gapi.client.sheets.spreadsheets.values.append({
        spreadsheetId: spreadsheetId,
        range: sheetName + "!A1:J1",
        valueInputOption: "USER_ENTERED",
        resource: body
    }).then(() => {
    });
}

function appendToLogSheet(postData) {
    appendToSheet(postData, "Všetky záznamy");
}

function appendToMainSheet(postData) {
    appendToSheet(postData, "Záznamy");
}

//delete row from firebase database
function deleteData(id, key) {
    firebase.database().ref('records/' + key).remove();
    $("#row" + id).remove();
}
